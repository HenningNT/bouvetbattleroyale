﻿using System;

namespace Pingutu
{
    public class CommandPayload
    {
        public Guid matchId { get; set; }
        public int mapWidth { get; set; }
        public int mapHeight { get; set; }
        public int wallDamage { get; set; }
        public int penguinDamage { get; set; }
        public int weaponDamage { get; set; }
        public int visibility { get; set; }
        public int weaponRange { get; set; }
        public Penguin you { get; set; }

        public Penguin[] enemies { get; set; }
        public Wall[] walls { get; set; }
        public BonusTile[] bonusTiles { get; set; }

        public int suddenDeath { get; set; }
    }
}
