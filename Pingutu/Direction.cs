﻿namespace Pingutu
{
    public static class Direction
    {
        public const string Top = "top";
        public const string Bottom = "bottom";
        public const string Left = "left";
        public const string Right = "right";
    }
}
