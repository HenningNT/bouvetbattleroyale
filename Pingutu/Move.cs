﻿namespace Pingutu
{
    public static  class Move
    {
        public const string RotateLeft = "rotate-left";
        public const string RotateRight = "rotate-right";
        public const string Advance = "advance";
        public const string Retreat = "retreat";
        public const string Shoot = "shoot";
        public const string Pass = "pass";
    }
}
