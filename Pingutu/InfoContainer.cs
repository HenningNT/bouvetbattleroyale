﻿namespace Pingutu
{
    public static partial class Info
    {
        public class InfoContainer
        {
            public InfoContainer(string name, string team)
            {
                this.name = name;
                this.team = team;
            }

            public string name { get; set; }
            public string team { get; set; }
        }
    }
}
