﻿namespace Pingutu
{
    public class BonusTile
    {
        public int x { get; set; }
        public int y { get; set; }
        public string type { get; set; }
        public int value { get; set; }
    }
}
