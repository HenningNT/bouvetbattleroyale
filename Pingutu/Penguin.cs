﻿namespace Pingutu
{
    public class Penguin
    {
        public string direction { get; set; }
        public int x { get; set; }
        public int y { get; set; }
        public int strength { get; set; }
        public int ammo { get; set; }
        public string status { get; set; }
        public int targetRange { get; set; }
        public int weaponRange { get; set; }
        public int weaponDamage { get; set; }
    }
}
