using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;

namespace Pingutu
{
    public static partial class Command
    {
        [FunctionName("command")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            var json = await req.Content.ReadAsStringAsync();
            var command = JsonConvert.DeserializeObject<CommandPayload>(json);

            log.Info("command: " + json);

            string response = "";

            Point dest = new Point(10, 10);
            var checkBehind = IsThereAEnemyBehindMe(command);
            if (checkBehind == "runAway")
            {
                var jsonToRetturn = JsonConvert.SerializeObject(new ReplyContainer
                {
                    command = "advance"
                });
                log.Info("response: " + jsonToRetturn);
                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(jsonToRetturn, Encoding.UTF8, "application/json")
                };
            }
            var check = IsEnemyInFrontOfUs(command);

            if (check == "shoot")
                response = Move.Shoot;
            else if (check == "retreat")
                response = "retreat";
            else if (check == "kill")
            {
                dest = new Point(command.enemies.First().x, command.enemies.First().y);
                response = GoTo(dest, command);
            }
            else
            {
                dest = TargetSelector(command);
                response = GoTo(dest, command);
            }
            var result = new ReplyContainer
            {
                command = response
            };

            var jsonToReturn = JsonConvert.SerializeObject(result);
            log.Info("response: " + jsonToReturn);
            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(jsonToReturn, Encoding.UTF8, "application/json")
            };
        }

        private static string IsEnemyInFrontOfUs(CommandPayload command)
        {
            if (command.enemies == null) return "move";
            var me = command.you;
            var enemy = command.enemies.First();

            if (enemy.weaponRange > me.weaponRange) return Move.Retreat;
            //if (!IsEnemyFacingUs(me, enemy))
            if (Math.Sqrt(Math.Pow(me.x - enemy.x, 2) + Math.Pow(me.y - enemy.y, 2)) > enemy.weaponRange) return "move";

            switch (command.you.direction)
            {
                case Direction.Top:
                    if (enemy.x == me.x && enemy.y < me.y) return Move.Shoot;
                    break;
                case Direction.Bottom:
                    if (enemy.x == me.x && enemy.y > me.y) return Move.Shoot;
                    break;
                case Direction.Left:
                    if (enemy.y == me.y && enemy.x < me.x) return Move.Shoot;
                    break;
                case Direction.Right:
                    if (enemy.y == me.y && enemy.x > me.x) return Move.Shoot;
                    break;
                default:
                    return "move";
            }
            return "kill";
        }

        private static bool IsEnemyFacingUs(Penguin me, Penguin enemy)
        {
            if (Math.Sqrt(Math.Pow(me.x - enemy.x, 2) + Math.Pow(me.y - enemy.y, 2)) > enemy.weaponRange) return false;

            switch (enemy.direction)
            {
                case Direction.Top:
                    return (me.y < enemy.y);
                case Direction.Bottom:
                    return (me.y > enemy.y);
                case Direction.Left:
                    return (me.x < enemy.x);
                case Direction.Right:
                    return (me.x > enemy.x);
                default:
                    return false;
            }
        }

        private static Point TargetSelector(CommandPayload command)
        {
            var me = command.you;
            
            

            var closest = new BonusTile();
            double dist = 42;

            // Find closest bonus
            if (command.bonusTiles.Any())
            {
                BonusTile bonusTile = command.bonusTiles[0];
                // Find closest bonus
                foreach (var bonus in command.bonusTiles)
                {
                    var distance = Math.Sqrt(Math.Pow(me.x - bonus.x, 2) + Math.Pow(me.y - bonus.y, 2));
                    if (distance < dist)
                    {
                        dist = distance;
                        closest = bonus;
                    }
                }
            }
            if (command.enemies == null)
                return new Point(closest.x, closest.y);

            var enemy = command.enemies.First();
            var enemyDistance = Math.Sqrt(Math.Pow(me.x - enemy.x, 2) + Math.Pow(me.y - enemy.y, 2));

            if (dist < enemyDistance)
                return new Point(closest.x, closest.y);
            else
                return new Point(enemy.x, enemy.y);
        }

        private static string GoTo(Point dest, CommandPayload command)
        {
            var me = command.you;

            if (dest.X < 0) dest.X = 0;
            if (dest.X > command.mapWidth) dest.X = command.mapWidth;
            if (dest.Y < 0) dest.Y = 0;
            if (dest.Y > command.mapHeight) dest.Y = command.mapHeight;

            dest = RouteSelect(dest, command);

            // Do we need to turn
            switch (me.direction)
            {
                case Direction.Top:
                    // Are we going up?
                    if (dest.Y < me.y)
                    {
                        // Is there something in front of us
                        if (IsThereWallInFrontOfMe(command))
                            return Move.Shoot;
                        else
                        {
                            return Move.Advance; // Yes, go up
                        }
                    }
                    else if (dest.Y > me.y)
                    {
                        // Is there something in front of us
                        if (IsThereWallInFrontOfMe(command))
                            return Move.RotateRight;
                        else if (IsThereAWallBehindMe(command))
                        {
                            return Move.RotateRight;
                        }
                        else
                        {
                            return Move.Retreat; // NO, go down
                        }
                    }
                    // We are on the same y coord as target, must turn to face target
                    if (dest.X < me.x)
                        return Move.RotateLeft; // Must turn left
                    if (dest.X > me.x)
                        return Move.RotateRight; // Must run right
                    break;

                case Direction.Bottom:
                    // Are we going up?
                    if (dest.Y > me.y)
                    {
                        // Is there something in front of us
                        if (IsThereWallInFrontOfMe(command))
                            return Move.Shoot;
                        else
                            return Move.Advance; // Yes, go down
                    }
                    if (dest.Y < me.y)
                    {
                        // Is there something in front of us
                        if (IsThereWallInFrontOfMe(command))
                            return Move.RotateRight;
                        else
                        if (IsThereAWallBehindMe(command))
                        {
                            return Move.RotateRight;
                        }
                        else
                        {
                            return Move.Retreat; // NO, go down
                        }
                    }
                    // We are on the same y coord as target, must turn to face target
                    if (dest.X < me.x)
                        return Move.RotateRight; // Must turn left
                    if (dest.X > me.x)
                        return Move.RotateLeft; // Must run right
                    break;

                case Direction.Left:
                    // Are we going left?
                    if (dest.X < me.x)
                    {
                        // Is there something in front of us
                        if (IsThereWallInFrontOfMe(command))
                            return Move.Shoot;
                        else
                            return Move.Advance; // Yes, go left
                    }
                    if (dest.X > me.x)
                    {
                        // Is there something in front of us
                        if (IsThereWallInFrontOfMe(command))
                            return Move.RotateRight;
                        else
                        if (IsThereAWallBehindMe(command))
                        {
                            return Move.RotateRight;
                        }
                        else
                        {
                            return Move.Retreat; // NO, go down
                        }
                    }
                    // We are on the same x coord as target, must turn to face target
                    if (dest.Y < me.y)
                        return Move.RotateRight; // Must turn left
                    if (dest.Y > me.y)
                        return Move.RotateLeft; // Must run right
                    break;

                case Direction.Right:
                    // Are we going right?
                    if (dest.X > me.x)
                    {
                        // Is there something in front of us
                        if (IsThereWallInFrontOfMe(command))
                            return Move.Shoot;
                        else
                            return Move.Advance; // Yes, go right
                    }
                    if (dest.X < me.x)
                    {
                        // Is there something in front of us
                        if (IsThereWallInFrontOfMe(command))
                            return Move.RotateRight;
                        else
                        if (IsThereAWallBehindMe(command))
                        {
                            return Move.RotateRight;
                        }
                        else
                        {
                            return Move.Retreat; // NO, go down
                        }
                    }
                    // We are on the same x coord as target, must turn to face target
                    if (dest.Y < me.y)
                        return Move.RotateLeft; // Must turn left
                    if (dest.Y > me.y)
                        return Move.RotateRight; // Must run right
                    break;
                default:
                    return Move.Pass;
            }

            if (IsThereAWallBehindMe(command))
            {
                return Move.RotateRight;
            }
            else
            {
                return Move.Retreat; // NO, go down
            }
        }

        private static Point RouteSelect(Point dest, CommandPayload command)
        {
            var me = command.you;
            // Check if we already are lined up
            if ((me.x == dest.X) || (me.y == dest.Y))
                return dest;

            var RouteXYPoints = new List<Point>();
            var RouteYXPoints = new List<Point>();
            int startX = dest.X < me.x ? dest.X : me.x;
            int stopX = dest.X > me.x ? dest.X : me.x;
            int startY = dest.Y < me.y ? dest.Y : me.y;
            int stopY = dest.Y > me.y ? dest.Y : me.y;

            // Figure out route X-Y
            for (int x = startX; x < stopX; x++)
            {
                RouteXYPoints.Add(new Point(x, me.y));
            }
            for (int y = startY; y < stopY; y++)
            {
                RouteXYPoints.Add(new Point(dest.X, y));
            }
            

            // Figure out route Y-X 
            for (int y = startY; y < stopY; y++)
            {
                RouteYXPoints.Add(new Point(me.x, y));
            }
            for (int x = startX; x < stopX; x++)
            {
                RouteYXPoints.Add(new Point(x, dest.Y));
            }

            RouteXYPoints.Remove(dest);
            RouteYXPoints.Remove(dest);
            RouteXYPoints.Remove(new Point(me.x, me.y));
            RouteYXPoints.Remove(new Point(me.x, me.y));

            // Figure out which one has the most walls
            int countXY = 0, countYX = 0;
            foreach (var wall in command.walls)
            {
                if (RouteXYPoints.Any(point => point.X == wall.x && point.Y == wall.y))
                    countXY++;
                if (RouteYXPoints.Any(point => point.X == wall.x && point.Y == wall.y))
                    countYX++;
            }
            if (countXY == countYX) return dest;

            if (countXY > countYX)
            {
                // First move horizontally
                if (dest.X > me.x)
                    return new Point(me.x + 1, me.y);
                else
                    return new Point(me.x - 1, me.y);
            }

            // Else first move vertically
            if (dest.Y > me.y)
                return new Point(me.x, me.y + 1);
            else
                return new Point(me.x, me.y - 1);
        }

        private static bool IsThereWallInFrontOfMe(CommandPayload command)
        {
            var me = command.you;

            switch (command.you.direction)
            {
                case Direction.Top:
                    return command.walls.Any(wall => wall.x == me.x && wall.y == me.y - 1);
                case Direction.Bottom:
                    return command.walls.Any(wall => wall.x == me.x && wall.y == me.y + 1);
                case Direction.Left:
                    return command.walls.Any(wall => wall.y == me.y && wall.x == me.x - 1);
                case Direction.Right:
                    return command.walls.Any(wall => wall.y == me.y && wall.x == me.x + 1);
                default:
                    return false;
            }
        }

        private static bool IsThereAWallBehindMe(CommandPayload command)
        {
            var me = command.you;

            switch (command.you.direction)
            {
                case Direction.Top:
                    return command.walls.Any(wall => wall.x == me.x && wall.y == me.y + 1);

                case Direction.Bottom:
                    return command.walls.Any(wall => wall.x == me.x && wall.y == me.y - 1);
                case Direction.Left:
                    return command.walls.Any(wall => wall.y == me.y && wall.x == me.x + 1);
                case Direction.Right:
                    return command.walls.Any(wall => wall.y == me.y && wall.x == me.x - 1);

                default:
                    return false;
            }
        }

        private static string IsThereAEnemyBehindMe(CommandPayload command)
        {
            if (command.enemies == null) return "move";
            var me = command.you;
            var enemy = command.enemies.First();

            if (Math.Sqrt(Math.Pow(me.x - enemy.x, 2) + Math.Pow(me.y - enemy.y, 2)) > enemy.weaponRange) return "move";

            switch (command.you.direction)
            {
                case Direction.Top:
                    if (enemy.x == me.x && enemy.y > me.y) return "runAway";
                    break;
                case Direction.Bottom:
                    if (enemy.x == me.x && enemy.y < me.y) return "runAway";
                    break;
                case Direction.Left:
                    if (enemy.y == me.y && enemy.x > me.x) return "runAway";
                    break;
                case Direction.Right:
                    if (enemy.y == me.y && enemy.x < me.x) return "runAway";

                    break;
                default:
                    return "move";
            }
            return "move";
        }
    }
}
